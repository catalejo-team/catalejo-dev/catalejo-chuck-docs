# Guia para el uso de ésta documentación

La documentación presentada a continuación está escrita en MarkDown y puede
ser visualizada en HTMl.

  * Para editar y/o agregar más documentación puede hacerlo en el directorio **docs/**.

  * El `Makefile` es responsable de las tareas de construcción y visualización de la información.
    Puede ver ayudas digitando `make`.

  * El proceso de visualización de la información es el siguiente:

    1.  Abra una terminal y ejecute `make serve`.

    2.  Abra el navegador y entre a la siguiente dirección web `localhost:8000`.

## Instalación de MkDocs

### En Linux

Crear un entorno, si usa miniconda:

```bash
conda create --name work
conda activate work
conda install -c anaconda pip
```

```bash
pip install mkdocs
pip install mkdocs-material
pip install mkdocs-with-pdf
pip install mkdocs-glightbox
```

* https://github.com/mkdocs/mkdocs.git (no especifica)
* https://github.com/shauser/mkdocs-pdf-export-plugin (python3)
* https://github.com/squidfunk/mkdocs-material.git (python3)
* https://github.com/apenwarr/mkdocs-exclude.git

## ¿ Más temas ?

[Temas](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)

## Recomendaciones

Haz uso de un optimizador de imagenes para disminuir el tamaño de estas
y mejorar el rendimiento, por ejemplo:

[TinyPNG](https://tinypng.com/)

Atentamente:

Catalejo+ Team

johnnycubides@catalejoplus.com
