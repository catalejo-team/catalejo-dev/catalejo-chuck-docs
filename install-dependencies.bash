#!/bin/bash

CONDA=~/miniconda3/bin/conda

dependencies() {
	sudo apt update
	sudo apt install \
		chromium \
		zenity \
		-y
}

iconda() {
	cd Downloads
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	bash Miniconda3-latest-Linux-x86_64.sh # Seguir las instrucciones y reiniciar la terminal
	rm -i Miniconda3-latest-Linux-x86_64
}

create() {
	ENV=work
	$CONDA create --name $ENV
}

activate() {
	ENV=work
	$CONDA activate $ENV
}

deactivate() {
	$CONDA deactivate
}

install() {
	$CONDA install -c anaconda pip
	pip3 install colorama
	pip3 install mkdocs-with-pdf
	pip install mkdocs-material
	pip install mkdocs-glightbox
	pip install -r requirements.txt
}

help() {
	echo "
dependencies -> instalar dependencias
iconda -> instalar conda
create -> crear variable de entorno $ENV
activate -> activar entorno $ENV
install -> instalar paquetes
  "
}

if [[ -v 1 ]]; then
	$1
else
	help
fi
