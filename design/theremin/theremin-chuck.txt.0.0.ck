Clarinet clarinete1;


1 :: second => dur compas;
compas/2 => dur Negra; //Cuarta
4*Negra => dur Redonda; //Entera
2*Negra => dur Blanca; //Media
Negra/2 => dur Corchea; //Octava
Negra/4 => dur Semicorchea; //Dieciseisava
Negra/8 => dur Fusa; //Treintaidosava
Negra/16 => dur Semifusa; //Sesentacuatroava
clarinete1 => dac;
1 => clarinete1.noteOn;
263 => clarinete1.vibratoFreq;
(48) => Std.mtof => clarinete1.freq;
(Negra) => now;
(60) => Std.mtof => clarinete1.freq;
(Negra) => now;
