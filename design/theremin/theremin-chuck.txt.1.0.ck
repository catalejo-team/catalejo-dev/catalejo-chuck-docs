OscIn osc_recibir;
Clarinet clarinete1;
OscMsg osc_mensaje;
int notas;


6449 => osc_recibir.port;
osc_recibir.listenAll();
1 :: second => dur compas;
compas/2 => dur Negra; //Cuarta
4*Negra => dur Redonda; //Entera
2*Negra => dur Blanca; //Media
Negra/2 => dur Corchea; //Octava
Negra/4 => dur Semicorchea; //Dieciseisava
Negra/8 => dur Fusa; //Treintaidosava
Negra/16 => dur Semifusa; //Sesentacuatroava
clarinete1 => dac;
1 => clarinete1.noteOn;
263 => clarinete1.vibratoFreq;
while (true)
{
  osc_recibir => now;
  while(osc_recibir.recv(osc_mensaje))
  {
    (osc_mensaje.getInt(0)) => notas;
    notas => Std.mtof => clarinete1.freq;
    <<<notas>>>;
    (1 :: ms) => now;
  }
}
