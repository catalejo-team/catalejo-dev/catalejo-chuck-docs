.ONESHELL:

SHELL=/bin/bash

# Enviroment conda to activate
ENV=work
CONDA_ACTIVATE = source $$($$CONDA_EXE info --base)/etc/profile.d/conda.sh ; conda activate; conda activate $(ENV)

c?=mkdocs-material.yml

help:
	@printf "########################\n"\
	"#        MKDOCS        #\n"\
	"########################\n\n"\
	"Éste directorio \"docs/\" es de documentación "\
	"el cual está escrito en markdown y puede ser compilado para html.\n\n"\
	"Instrucciones:\n"\
	"\t1°: Debe tener instalado mkdocs y los demás plugins (ver el README.md)\n"\
	"\t- link oficial: https://www.mkdocs.org/\n"\
	"\t- link github: https://github.com/mkdocs/mkdocs\n"\
	"\n"\
	"Comandos:\n"\
	"\tserve:\tInicia un servidor web dinámico para la previsualización\n"\
	"\tview:\tMuestra una vista HTML previa en el navegador\n"\
	"\tbuild:\tConstruye HTML definitivo para empotrar en algún servidor web\n"\
	"\tclear:\tBorra contenido en el directorio build/\n\n"\
	"Atentos saludos:\n"\
	"\tEquipo Catalejo+\n\n"\
	"Ejemplos de uso:\n"\
	"\tmake build\n"\
	"\tmake build c=file.ylm\n"\
	"\tmake serve\n"\
	"\tmake serve c=file.ylm\n"

s: serve-chuck-web
b: build-chuck-web
p: pdf-view

create-yml-chuck:
	cat ./cfg-yml/site-chuck-web.yml ./cfg-yml/nav.yml ./cfg-yml/plugin.yml > $c

create-yml-chuck-pdf:
	cat ./cfg-yml/site-chuck-web.yml ./cfg-yml/nav.yml ./cfg-yml/plugin-pdf.yml > $c

serve:
	mkdocs serve -f $(c)

serve-chuck-web: create-yml-chuck
	$(CONDA_ACTIVATE)
	mkdocs serve -f $(c)

view:
	x-www-browser localhost:8000
	@printf "Sino se ha lanzado el navegador con el contenido "\
	"everifique que en otra terminal este corriendo el comando serve (make serve) "\
	"y luego refresque el navegador (F5).\n"

build:
	mkdocs build -c -f $(c)

build-chuck-web: create-yml-chuck-pdf
	$(CONDA_ACTIVATE)
	mkdocs build -v -c -f $(c)

pdf-view:
	evince build/pdf/document.pdf &

clean:
	rm -rf build/*

.PHONY: build
