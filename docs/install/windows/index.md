# Catalejo Editor

## Instalar Catalejo Editor en Windows

### Descargar la aplicación comprimida catalejo-win32.zip

* En su navegador de preferencia diríjase al enlace [https://sourceforge.net/projects/catalejo-editor-desktop/](https://sourceforge.net/projects/catalejo-editor-desktop/)
e inicie la descarga de *Catalejo Editor* desde el botón de descarga (botón verde). 

![sourceforge](img/sourceforge.PNG)

* El sistema reconocerá automáticamente su sistema y realizará la descarga del la aplicación para su sistema.

![iniciar descarga](img/descargar.PNG)

* El proceso de descarga durará algunos minutos.

![descargando](img/descargando.PNG)

* Proceso de descarga terminado

![Descarga terminada](img/terminada-descarga.PNG)

### Mover el catalejo-win32.zip

* Desde el navegador puede mostrar la carpeta donde fue descargada la aplicación

![abrir caperta donde está alojado el archivo comprimido](img/abrirCarpetaDescarga.PNG)

* Desde el explorador de archivos de Windows podrá ubicar catalejo-win32.zip

![ubicación del archivo comprimido](img/directorioDescarga.PNG)

* Traslade el archivo.zip a otro lugar para descomprimir, como ejemplo, será trasladado a al
directorio *Documentos*

![moviendo con la opción de cortar](img/mover.PNG)

* Con clic derecho en el gestor de archivos pegue el archivo en el destino seleccionado

![Pegar comprimido](img/pegar.PNG)

![Archivo pegado en el destino](img/pegado.PNG)

### Descomprimir el catalejo-win32.zip

* A continuación extraiga el contenido del archivo.zip

![Extrar aquí](img/extraeraqui.PNG)

* Al extraer el contenido, se genera un nuevo directorio llamado `catalejo-win32`

![Contenido extraido](img/extraido.PNG)

### Ejecutar la aplicación Catalejo Editor

* Al entrar al directorio descomprimido busque el ícono que tiene por nombre `nw` y dar en él
doble clic

![Ejecutando aplicación](img/ejecutarNW.PNG)

* Ya podrá interactuar con la aplicación

![Abriendo aplicación catalejo editor](img/openCatalejo.PNG)

## Mi primer Tono con Catalejo Editor y Chuck

* Para iniciar un proyecto en *Catalejo Editor* es importante crear un directorio donde se pueda alojar cada proyecto,
es decir, un directorio por proyecto, por ejemplo, se creará un directorio en `Documentos` llamado `miProyecto`

![Crear nuevo directorio](img/carpeta-proyecto.PNG)

![Carpeta proyecto creada](img/carpeta-proyecto-creada.PNG)

* Inicie la aplicación Catalejo Editor y en el botón al botón `¡Empieza!` dar clic, esto avanzará al botón `Proyectos`, dar clic, en proyectos seleccionar la opción `Chuck`
y en la ventana emergente dar clic en el botón `Seleccionar archivo`

![Proyectos](img/proyectos.PNG)

![Chuck proyectos](img/nuevoChuckProyecto.PNG)

![Creando un nuevo proyecto chuck](img/seleccionarUnArchicoProyectoChuc.PNG)

* Crear un nuevo archivo para guardar el código a realizar, en el ejemplo, `nuevoProyecto.txt`

![Nuevo proyecto chuck](img/nuevoroyectotTXT.PNG)

* Lo anterior abrirá un área de trabajo en Catalejo Editor

![Área de trabajo](img/areaDeTrabajoIniciada.PNG)

* Antes de editar el primer tono, se debe iniciar el servicio de chuck, dar clic en el ícono relacionado a chuck y en la ventana emergente, dar clic en el ícono asociado a`Iniciar chuck en este equipo`

![iniciando el servicio de chuck](img/iniciarChuckEnEsteEquipo.PNG)

* Por ser la primera vez que inicia chuck en éste equipo Windows pedirá permisos de ejecución, dar clic en permitir, y deberá cerrar la aplicación y volverla abrir realizando el mismo procedimiento
de esta sección, este permiso es solicitado únicamente la primera vez que abre Catalejo Editor, así que puede luego continuar con el siguiente paso.

![Permisos](img/permiTirAcceso.PNG)

