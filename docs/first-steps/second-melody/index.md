# Mi segunda melodía

## Notas en el piano

![piano](img/piano.jpg)


## Escala musical de SOL

![clave de sol](img/clave-de-sol.jpg)

## Octavas del piano

![notas piano](img/OCTAVAS_PIANO.png)

## Significado del número 60 en el tono

* Notación midi

--8<--
midi-es.md
--8<--

![Nota 60](img/midi-do4-60.png)


* Toca una melodía: Agrega más bloques hacia abajo ...


* Altera el ritmo: Cambia el número de segundos para escuchar qué sucede, ejemplo: 0.5, 0.1, 2.

* Cambiando el tiempo:

## Duración figuras musicales

[Metrónomo](https://www.musicca.com/es/metronomo)

A continuación podrás reconocer la duración de las figuras musicales:

![figuras musicales](img/la_duracion_de_las_figuras_musicales.png)

Pruba cambiando los PPM, qué pasa con valores de 400 o 80.


* Melodía a interpretar:

![estrellita](img/melodia-estrellita.jpg)




