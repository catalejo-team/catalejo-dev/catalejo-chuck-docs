# Mi primera melodía

## Mi primer tono

En catalejo editor en modo Blockly (programación por bloques)
construye este código, activa ChucK y dale clic en el botón de **Play**.

![mi primer tono](img/mi-primer-tono.png)

!!! Info "Para poder escuchar un sonido se requiere de dos conjuntos de bloques:"
    * Se requiere un tono que es representado en este caso por el número 60
    * Se requiere una duración que es representada por el bloque de 1 segundo.

!!! Warning "Si no se asigna la duración del tono la computadora tomará el tiempo más rápido posible por ella de ejecutar"
    


## Cambiando el tono

![Cambiando el tono a escuchar](img/cambiar-tono.png)


!!! Question "Prueba cambiando el número 60 por otros números y responde las siguientes preguntas:"
       * ¿Cómo se percibe el sonido con números menores a 60? 
       * ¿Cómo se percibe el sonido con números mayores a 60?

## Tocando una melodía

Para tocar una melodía se requiere ordenar otros tonos de manera descendente (de arriba a bajo),
cada tono nuevamente debe estar acompañada de su duración, ejemplo:

![Tocando la primera melodía](img/secuencia-de-tonos.png)

!!! example "Experimente creando su propia melodía"
       1. Reproduzca la melodía de ejemplo
       2. Cambie el tono de los bloques y escuche como suena

## Alterando el ritmo

En este caso basta con cambiar el valor del tiempo por un otro número, ejemplo:

![alterar el ritmo](img/alterar-el-ritmo.png)

!!! example "Experimente cambiando el ritmo"
       1. Reproduzca la melodía de ejemplo
       2. Cambie el tiempo de los bloques y escuche como suena

## Creando su propia melodía

!!! Tip "Construya una melodía en la cual tenga en cuenta lo siguiente:"
    1. Tenga más de tres tonos
    2. Altere el ritmo
    3. Enséñela a una persona y pregúntele qué opina sobre cómo suena.

