# Control de Flujo

## Estructura IF

![If statement](img/estructuras-de-control-if.drawio.png)

### Ejemplo IF en Blockly

![Blockly if](img/if-example-blockly-chuck.png)

## Estrutura ELSE

![ELSE statement](img/estructuras-de-control-else.drawio.png)

### Ejemplo de ELSE en Blockly

![Blockly ELSE](img/else-example-blockly-chuck.png)

## Estrutura ELSE IF

![ELSE IF](img/estructuras-de-control-else if.drawio.png)

### Ejemplo de ELSE IF en Blockly

![Blockly ELSE IF](img/else-if-example-blockly-chuck.png)

