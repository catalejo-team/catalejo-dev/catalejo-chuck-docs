<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-ohc4{background-color:#3166ff;color:#ffffff;text-align:center;vertical-align:top}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-igm7{background-color:#6200c9;color:#ffffff;font-weight:bold;text-align:center;vertical-align:top}
.tg .tg-sh07{background-color:#cbcefb;text-align:center;vertical-align:top}
.tg .tg-j6lv{background-color:#96fffb;text-align:center;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-baqh"></th>
    <th class="tg-igm7" colspan="12">Notas</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-ohc4">Octava</td>
    <td class="tg-sh07">Do</td>
    <td class="tg-sh07">Do#</td>
    <td class="tg-sh07">Re</td>
    <td class="tg-sh07">Re#</td>
    <td class="tg-sh07">Mi</td>
    <td class="tg-sh07">Fa</td>
    <td class="tg-sh07">Fa#</td>
    <td class="tg-sh07">Sol</td>
    <td class="tg-sh07">Sol#</td>
    <td class="tg-sh07">La</td>
    <td class="tg-sh07">La#</td>
    <td class="tg-sh07">Si</td>
  </tr>
  <tr>
    <td class="tg-j6lv">-1</td>
    <td class="tg-baqh">0</td>
    <td class="tg-baqh">1</td>
    <td class="tg-baqh">2</td>
    <td class="tg-baqh">3</td>
    <td class="tg-baqh">4</td>
    <td class="tg-baqh">5</td>
    <td class="tg-baqh">6</td>
    <td class="tg-baqh">7</td>
    <td class="tg-baqh">8</td>
    <td class="tg-baqh">9</td>
    <td class="tg-baqh">10</td>
    <td class="tg-baqh">11</td>
  </tr>
  <tr>
    <td class="tg-j6lv">0</td>
    <td class="tg-baqh">12</td>
    <td class="tg-baqh">13</td>
    <td class="tg-baqh">14</td>
    <td class="tg-baqh">15</td>
    <td class="tg-baqh">16</td>
    <td class="tg-baqh">17</td>
    <td class="tg-baqh">18</td>
    <td class="tg-baqh">19</td>
    <td class="tg-baqh">20</td>
    <td class="tg-baqh">21</td>
    <td class="tg-baqh">22</td>
    <td class="tg-baqh">23</td>
  </tr>
  <tr>
    <td class="tg-j6lv">1</td>
    <td class="tg-baqh">24</td>
    <td class="tg-baqh">25</td>
    <td class="tg-baqh">26</td>
    <td class="tg-baqh">27</td>
    <td class="tg-baqh">28</td>
    <td class="tg-baqh">29</td>
    <td class="tg-baqh">30</td>
    <td class="tg-baqh">31</td>
    <td class="tg-baqh">32</td>
    <td class="tg-baqh">33</td>
    <td class="tg-baqh">34</td>
    <td class="tg-baqh">35</td>
  </tr>
  <tr>
    <td class="tg-j6lv">2</td>
    <td class="tg-baqh">36</td>
    <td class="tg-baqh">37</td>
    <td class="tg-baqh">38</td>
    <td class="tg-baqh">39</td>
    <td class="tg-baqh">40</td>
    <td class="tg-baqh">41</td>
    <td class="tg-baqh">42</td>
    <td class="tg-baqh">43</td>
    <td class="tg-baqh">44</td>
    <td class="tg-baqh">45</td>
    <td class="tg-baqh">46</td>
    <td class="tg-baqh">47</td>
  </tr>
  <tr>
    <td class="tg-j6lv">3</td>
    <td class="tg-baqh">48</td>
    <td class="tg-baqh">49</td>
    <td class="tg-baqh">50</td>
    <td class="tg-baqh">51</td>
    <td class="tg-baqh">52</td>
    <td class="tg-baqh">53</td>
    <td class="tg-baqh">54</td>
    <td class="tg-baqh">55</td>
    <td class="tg-baqh">56</td>
    <td class="tg-baqh">57</td>
    <td class="tg-baqh">58</td>
    <td class="tg-baqh">59</td>
  </tr>
  <tr>
    <td class="tg-j6lv">4</td>
    <td class="tg-baqh">60</td>
    <td class="tg-baqh">61</td>
    <td class="tg-baqh">62</td>
    <td class="tg-baqh">63</td>
    <td class="tg-baqh">64</td>
    <td class="tg-baqh">65</td>
    <td class="tg-baqh">66</td>
    <td class="tg-baqh">67</td>
    <td class="tg-baqh">68</td>
    <td class="tg-baqh">69</td>
    <td class="tg-baqh">70</td>
    <td class="tg-baqh">71</td>
  </tr>
  <tr>
    <td class="tg-j6lv">5</td>
    <td class="tg-baqh">72</td>
    <td class="tg-baqh">73</td>
    <td class="tg-baqh">74</td>
    <td class="tg-baqh">75</td>
    <td class="tg-baqh">76</td>
    <td class="tg-baqh">77</td>
    <td class="tg-baqh">78</td>
    <td class="tg-baqh">79</td>
    <td class="tg-baqh">80</td>
    <td class="tg-baqh">81</td>
    <td class="tg-baqh">82</td>
    <td class="tg-baqh">83</td>
  </tr>
  <tr>
    <td class="tg-j6lv">6</td>
    <td class="tg-baqh">84</td>
    <td class="tg-baqh">85</td>
    <td class="tg-baqh">86</td>
    <td class="tg-baqh">87</td>
    <td class="tg-baqh">88</td>
    <td class="tg-baqh">89</td>
    <td class="tg-baqh">90</td>
    <td class="tg-baqh">91</td>
    <td class="tg-baqh">92</td>
    <td class="tg-baqh">93</td>
    <td class="tg-baqh">94</td>
    <td class="tg-baqh">95</td>
  </tr>
  <tr>
    <td class="tg-j6lv">7</td>
    <td class="tg-baqh">96</td>
    <td class="tg-baqh">97</td>
    <td class="tg-baqh">98</td>
    <td class="tg-baqh">99</td>
    <td class="tg-baqh">100</td>
    <td class="tg-baqh">101</td>
    <td class="tg-baqh">102</td>
    <td class="tg-baqh">103</td>
    <td class="tg-baqh">104</td>
    <td class="tg-baqh">105</td>
    <td class="tg-baqh">106</td>
    <td class="tg-baqh">107</td>
  </tr>
  <tr>
    <td class="tg-j6lv">8</td>
    <td class="tg-baqh">108</td>
    <td class="tg-baqh">109</td>
    <td class="tg-baqh">110</td>
    <td class="tg-baqh">111</td>
    <td class="tg-baqh">112</td>
    <td class="tg-baqh">113</td>
    <td class="tg-baqh">114</td>
    <td class="tg-baqh">115</td>
    <td class="tg-baqh">116</td>
    <td class="tg-baqh">117</td>
    <td class="tg-baqh">118</td>
    <td class="tg-baqh">119</td>
  </tr>
  <tr>
    <td class="tg-j6lv">9</td>
    <td class="tg-baqh">120</td>
    <td class="tg-baqh">121</td>
    <td class="tg-baqh">122</td>
    <td class="tg-baqh">123</td>
    <td class="tg-baqh">124</td>
    <td class="tg-baqh">125</td>
    <td class="tg-baqh">126</td>
    <td class="tg-baqh">127</td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
    <td class="tg-baqh"></td>
  </tr>
</tbody>
</table>
