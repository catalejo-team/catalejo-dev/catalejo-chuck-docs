# Variables y Operadores

## Variables

Las variables son nombres o identificadores de valores; los valores
son cosas que corresponden a un tipo de dato como es el caso de números, letras,
e inclusive instrumentos.

Para crear una variable se debe seleccionar en la caja de herramientas
el tipo de variable a crear, poner un nombre y asignar un valor, ejemplo:

![Creación de variables y asignación de valores](img/variables.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

En la imagen de arriba los números representan lo siguiente:

1. para crear una variable del tipo entero se debe ir a la catagoría
*Var datos* y en *Variable Name* se ha puesto el nombre de la variable
como **Nota**.
2. En *Variable Types* seleccionar *Entero* y dar clic en el botón de Ok.
3. En la categoría *Var datos* se han creado dos bloques que representan
la variable creada.
4. Como *nota* es una variable del tipo *Entero* desde la categoría
Matemáticas se le ha asignado un valor entero de 60.

En el lenguaje de Chuck esta variable se representa como sigue:

```py
int nota;

60 => nota;
```

!!! Info "Otros tipos de variables"
    En chuck-blockly hay distintos tipos de variables que se pueden ubicar en las categorías de Var datos, Var instrumento, Var oscilador y Var comunicación.

## Operadores

![Diferentes tipos de operadores](img/operadores.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}


```c linenums="1"
int nota1;
int nota2;
int nota3;

nota1 == nota2;

nota1 + nota2;

nota1 > nota2 || nota1 > nota3;
```
